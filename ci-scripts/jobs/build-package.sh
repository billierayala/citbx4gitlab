
CITBX_PACKAGE_NAME="ci-toolbox"

job_define() {
    bashopts_declare -n CI_COMMIT_TAG -l image-tag -d "Image tag" -t string -v "$(cat VERSION)-dev"
    citbx_export CI_COMMIT_TAG
}

job_main() {
    local pkgtype=${CI_JOB_NAME##*-}
    local version=${CI_COMMIT_TAG:-$(
        cat VERSION | tr -d '\n'
        if [ "$CI_COMMIT_REF_NAME" != "master" ]; then
            echo -n "-dev"
        fi
    )}
    local fpm_args=(
        -s dir
        -n $CITBX_PACKAGE_NAME
        --url https://gitlab.com/ercom/citbx4gitlab
        --description "CI toolbox for Gitlab CI"
        -m "ERCOM <support@ercom.fr>"
        --license "GPLv3"
        --vendor "ERCOM"
        --conflicts $CITBX_PACKAGE_NAME
        --provides $CITBX_PACKAGE_NAME
        --replaces $CITBX_PACKAGE_NAME
        --depends git
        --depends curl
        --depends tar
        --depends file
        --depends bzip2
        -a all
    )
    case "$pkgtype" in
        deb)
            rm -f artifacts/$CITBX_PACKAGE_NAME.deb
            fpm_args+=(
                --depends debianutils
                -v $version
                -t deb --deb-priority optional --category admin
                --deb-no-default-config-files
                -p artifacts/$CITBX_PACKAGE_NAME.deb --deb-compression xz
                --after-install packaging/scripts/postinst.deb
                --depends ca-certificates
            )
            ;;
        rpm)
            rm -f artifacts/$CITBX_PACKAGE_NAME.rpm
            fpm_args+=(
                --depends which
                -v ${version//-/_}
                -t rpm --rpm-os linux
                -p artifacts/$CITBX_PACKAGE_NAME.rpm  --rpm-compression xz
                --after-install packaging/scripts/postinst.rpm
            )
            ;;
        *)
            print_critical "Unsupported package type: '$pkgtype'"
            ;;
    esac
    mkdir -p artifacts build
    sed -E 's/^(CITBX_VERSION=).*$/\1'"$(cat VERSION)"'/g' ci-toolbox/ci-toolbox.sh > build/ci-toolbox.sh
    chmod +x build/ci-toolbox.sh
    fpm_args+=(
        packaging/root/=/
        build/ci-toolbox.sh=/usr/lib/ci-toolbox/ci-toolbox.sh
        ci-scripts/common.sh=/usr/lib/ci-toolbox/common.sh
        .ci-toolbox.properties=/etc/ci-toolbox/ci-toolbox.properties.default
        ci-toolbox/env-setup/=/usr/lib/ci-toolbox/env-setup/
        ci-toolbox/3rdparty/=/usr/lib/ci-toolbox/3rdparty/
        wrapper/ci-toolbox=/usr/bin/ci-toolbox
        wrapper/bashcomp=/usr/share/bash-completion/completions/ci-toolbox
    )
    fpm "${fpm_args[@]}"
}

job_after() {
    rm -rf build
}
